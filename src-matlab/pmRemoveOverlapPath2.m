function [ lat2, lon2 ] = pmRemoveOverlapPath2( lat1, lon1, lat2, lon2, disttol )
%pmRemoveOverlapPath takes two overlapping Lat Lon paths and returns NaNs
%in path 2 where it overlaps.

%This is an exact solution and will be very slow

%constants
refE = referenceEllipsoid('wgs84');

%Distance tolerance default
if ~exist('disttol','var')
    disttol=100;% meters
    %in cities 10 didn't dedup along some straight highways (where points
    %are sparser)
    %75 no good
    %100 seems to work
    
end

%derived
numPathpts=numel(lat2);
%numTreePts=numel(lat1);

%allocation
willDelete=false(1,numPathpts);

for i =1:numPathpts
    
    %calculate distance matrix
    [distM, ~]=distance(lat1,lon1,lat2(i),lon2(i),refE);
    
    if min(distM) <= disttol
        willDelete(i)=true;
    end
    
end

willDeleteTrimmed=willDelete;

%pass 2: add start-stop behavior to numpts
for i=1:numPathpts
    
    if i==1  %first point
        
        %avoid single hanging point
        if willDelete(i+1)
            willDeleteTrimmed(i)=true;
        end
        
    elseif i==numPathpts
        
        %avoid single hanging point
        if willDelete(i-1)
            willDeleteTrimmed(i)=true;
        end
        
    else
        
        %preserve the ending/starting points of a deletetion
        if (~willDelete(i-1) || ~willDelete(i+1))
            willDeleteTrimmed(i)=false;
        end
        
    end
    
end

%now do the deletion

lat2(willDeleteTrimmed)=nan;
lon2(willDeleteTrimmed)=nan;


end

