function [ mergedTree ] = pmMakeTree( pathArray )
%This function merges paths, eliminating duplicate segments to form a tree.

%constants
plotpath=true;  %plot all the paths (debug)
refE=referenceEllipsoid('wgs84');

%var inits
mergedTree=struct;

%derived vars
numPaths=numel(pathArray);
numPathsnotEmpty=sum(~cellfun('isempty',pathArray));


if plotpath
    figure
    hold on
end

validPathYet=false;
for p = 1:numPaths
    
    disp(['Merging path ' num2str(p) ' of ' num2str(numPaths)])
    
    if ~isempty(pathArray{p})
        
        [ pLat, pLong ] = gmDecodeandMergeEncPolylines( pathArray{p}.pathPolyLineArray);
        
        if plotpath
            scatter(pLong,pLat)
            title('Input Points')
            axis square
        end
        
        if ~validPathYet %no duplicates for the first path
            mergedTree.lat = pLat;
            mergedTree.long = pLong;
            
            validPathYet=true;
        else
            %now do dupe removal
            
            %pmRemoveOverlapPath3 is optimized and will only work not near
            %the poles. Use Path2 for exact.
            [ pLat, pLong ] = pmRemoveOverlapPath3( mergedTree.lat, mergedTree.long, pLat, pLong);
            
            %append the duplicate removed path
            mergedTree.lat=[mergedTree.lat nan pLat];
            mergedTree.long=[mergedTree.long nan pLong];
            
        end
    end
    
end

disp('pmMakeTree finished path merging')

%remove consecutive NaNs
I=all(isnan(mergedTree.lat),1);
mergedTree.lat(strfind(I(:)',[1,1])) = [];

I=all(isnan(mergedTree.long),1);
mergedTree.long(strfind(I(:)',[1,1])) = [];

if plotpath
    figure
    I = ~isnan(mergedTree.lat) & ~isnan(mergedTree.long);
    scatter(mergedTree.long(I),mergedTree.lat(I));
    title('Merged points')
    axis square

    
end

%split up the paths
[mergedTree.branchLat, mergedTree.branchLong]=polysplit(mergedTree.lat,mergedTree.long);

mergedTree.numTreeBranches=numel(mergedTree.branchLat);

if plotpath
    
    figure
    hold on
    for i=1:mergedTree.numTreeBranches
        
        plot(mergedTree.branchLong{i},mergedTree.branchLat{i});
        
    end
    title('Merged and split branches');
    axis square
    
end

%now calc the length of the combined tree
mergedTree.branchDist=zeros(1, mergedTree.numTreeBranches);

for i=1:mergedTree.numTreeBranches
    
    for p=1:numel(mergedTree.branchLat{i})-1
        
        mergedTree.branchDist(i)=mergedTree.branchDist(i)+...
            distance(mergedTree.branchLat{i}(p), mergedTree.branchLong{i}(p),...
            mergedTree.branchLat{i}(p+1), mergedTree.branchLong{i}(p+1),...
            refE)./1000; %units=km
        
    end
    
end

mergedTree.totalLength=sum(mergedTree.branchDist);

disp('Finished pmMakeTree')


end
