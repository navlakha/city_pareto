function [  ] = pmWriteCSVTrafficStatsForParti( abbr )
%This function outputs a .csv file for each city with traffic stats,
%utilized in the ParTI analysis. Input is a city abbreviation (string). The
%function appends over repeated runs to the same csv file. The paths are
%read from the .mat file containing the pathArray structure.

%Outputs
%For each city, the following is output:
%
%abbr = Abbreviation (string)
%R = Always 'm'
%H = Always 'h'
%
%These variables are averaged over all paths:
%AveSpeed = Average speed without traffic, km/h
%AveTime = Average time to travel without traffic, min
%AveNumManuevers = Average number of manuevers, as returned by Google Maps
%
%Not all paths have traffic information. The following are averaged over
%only paths where Google Maps returns traffic:
%AveSpeedInTraff = Average speed in traffic, km/h
%AveTimeInTraff = Average time to travel with traffic, min
%ExcessTravTimeFactor = AveTimeInTraff / (AveTime for those paths with
%traffic information)
%TrafficSpeedFactor = AveSpeedInTraff / (AveSpeed for those paths with
%traffic information)

%params
verString='v5';
outputFilename='trafficstats.csv';

%constants and fixup
abbr=lower(abbr);

%init vars
dataArray=[];

disp(['pmWriteCSVTrafficStats processing ' abbr]);

if ~exist(outputFilename,'file')
    %write the header
    disp(['Starting new file ' outputFilename])
    fh=fopen(outputFilename,'w');
    fprintf(fh,['#abbr, R, H, AveSpeed, AveSpeedInTraff,'...
        'AveTime, AveTimeInTraff, ExcessTravTimeFactor, TrafficSpeedFactor,'...
        'AveNumManuevers' '\n']);
else
    fh=fopen(outputFilename,'a');
end




radString='rand';
fnameRadString='R';
radStringforPathArray='randTrimmed';


filename=[abbr '_' fnameRadString '_' 'h' '_' verString '.mat'];
load(filename,'-regexp','^(?!r$).*')

varNamepathArray=['pathArray' upper(abbr) radStringforPathArray];

cmd=['pathArray = ' varNamepathArray ';'];
eval(cmd);

numPaths=0;
numPathsWithTraffic=0;
travelMinutesAllPoints=0;
travelKmAllPoints=0;

travelKmWithTraffic=0;
travelMinutesinTraffic=0;

numManuevers=0;

travelMinutesNoTrafficForPointsWithTrafficData =0;

for i=1:numel(pathArray)
    %for each nonempty path
    if isempty(pathArray{i})
        continue
    end
    
    numPaths=numPaths+1;
    travelMinutesAllPoints=travelMinutesAllPoints + pathArray{i}.travelTime;
    travelKmAllPoints=travelKmAllPoints + pathArray{i}.pathDist;
    
    numManuevers=numManuevers+sum(~cellfun(@isempty,pathArray{i}.pathManueverArray));
    
    %Not all paths have traffic model
    PAtravelTimeinTraffic=pathArray{i}.travelTimeTraffic;
    if ~isnan(PAtravelTimeinTraffic)
        travelMinutesinTraffic=travelMinutesinTraffic + pathArray{i}.travelTimeTraffic;
        numPathsWithTraffic=numPathsWithTraffic+1;
        travelKmWithTraffic=travelKmWithTraffic+pathArray{i}.pathDist;
        
        travelMinutesNoTrafficForPointsWithTrafficData = travelMinutesNoTrafficForPointsWithTrafficData + pathArray{i}.travelTime;
    end
    
end

%Now compute the stats

%'#abbr, R, H, AveSpeed, AveSpeedInTraff, AveTime, AveTimeInTraff, ExcessTravTimeFactor, TrafficSpeedFactor \n'

aveSpeed=travelKmAllPoints/(travelMinutesAllPoints./60); %km/h

aveSpeedInTraff=travelKmWithTraffic/(travelMinutesinTraffic./60); %km/h
aveSpeedForPointsWithTraffic=travelKmWithTraffic/(travelMinutesNoTrafficForPointsWithTrafficData./60);

aveTime=travelMinutesAllPoints./numPaths; %min/trip
aveTimeInTraff=travelMinutesinTraffic./numPathsWithTraffic; %min/trip

aveTimeForPointsWithTraffic=travelMinutesNoTrafficForPointsWithTrafficData./numPathsWithTraffic;

excessTravTimeFactor=aveTimeInTraff./aveTimeForPointsWithTraffic;
trafficSpeedFactor=aveSpeedInTraff./aveSpeedForPointsWithTraffic;

aveNumManuevers=numManuevers./numPaths;


%Write them out
rStr='M';

outstring=[ abbr ',' rStr ',' 'h' ','...
    num2str(aveSpeed) ',' num2str(aveSpeedInTraff) ','...
    num2str(aveTime) ',' num2str(aveTimeInTraff) ','...
    num2str(excessTravTimeFactor) ',' num2str(trafficSpeedFactor) ','...
    num2str(aveNumManuevers) '\n'];

fprintf(fh,outstring);



fclose(fh);
end

