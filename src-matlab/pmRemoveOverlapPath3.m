function [ lat2, lon2 ] = pmRemoveOverlapPath3( lat1, lon1, lat2, lon2, disttol )
%pmRemoveOverlapPath takes two overlapping Lat Lon paths and returns NaNs
%in path 2 where it overlaps.

%this is a new optimized version of OverlapPath2
%It applies a degree based prescreen to minimize calls to distance().

%If disttol is big, or you are near the poles, >80 lat, then it may falsely
%screen out close points.

%constants
refE = referenceEllipsoid('wgs84');

%Distance tolerance default
if ~exist('disttol','var')
    disttol=100;% meters
    %in cities 10 didn't dedup along some straight highways (where points
    %are sparser)
    %75 no good
    %100 seems to work
    
end

%derived
numPathpts=numel(lat2);

%allocation
willDelete=false(1,numPathpts);

for i =1:numPathpts
    
    %the point to compare against
    lat2Point=lat2(i);
    lon2Point=lon2(i);
    
    %estimate the acceptable lat/lon difference in degrees for 10x the
    %tolerance
    latdiff=(disttol*10)./111111;    %1 degree ~111 km
    londiff=(disttol*10)./(111111.*cos(lat2Point*(pi/180)));
    
    %compare the lat/long diff and keep the small changes
    latIsSmall=abs(lat2Point - lat1) < latdiff;
    lonIsSmall=abs(lon2Point - lon1) < londiff;
    distIsSmall=latIsSmall & lonIsSmall;
    
    %trim
    lat1Trimmed=lat1(distIsSmall);
    lon1Trimmed=lon1(distIsSmall);
    
    %calculate distance matrix
    [distM, ~]=distance(lat1Trimmed,lon1Trimmed,lat2Point,lon2Point,refE);
    
    if min(distM) <= disttol
        willDelete(i)=true;
    end
    
end

willDeleteTrimmed=willDelete;

%pass 2: add start-stop behavior to numpts
for i=1:numPathpts
    
    if i == 1 && numPathpts == 1 %single point
        
        willDeleteTrimmed(i)=true;
        
    elseif i==1  %first point
        
        %avoid single hanging point
        if willDelete(i+1)
            willDeleteTrimmed(i)=true;
        end
        
    elseif i==numPathpts
        
        %avoid single hanging point
        if willDelete(i-1)
            willDeleteTrimmed(i)=true;
        end
        
    else
        
        %preserve the ending/starting points of a deletetion
        if (~willDelete(i-1) || ~willDelete(i+1))
            willDeleteTrimmed(i)=false;
        end
        
    end
    
end

%now do the deletion

lat2(willDeleteTrimmed)=nan;
lon2(willDeleteTrimmed)=nan;


end
