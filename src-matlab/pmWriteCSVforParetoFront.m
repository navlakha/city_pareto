function [  ] = pmWriteCSVforParetoFront( pathArray, mergedTree, filename )
%Given a pathArray and a mergedTree structure from the .mat files, outputs
%a .csv containing the relevant data for the Python pareto front analysis
%code

% All output coordinates and lengths are in Euclidian space, km units.
%
% For each travel path, outputs:
% (x,y) location of downtown
% (dx,dy) Delta location of each point from downtown
% straight-line distance (r)
% path travel distance
%
% Also provides summary data consisting of:
% Total Travel distance over all paths
% Length of all roads
% Total normalized travel distance (path travel distance/r)
% number of paths

outMatrix=[];
totalPathDist=0;
sumNormTravelDist=0;

for i=1:numel(pathArray)
    
    spA=pathArray{i};
    
    if ~isempty(spA)
        outMatrix(end+1,:)=[0 0 spA.pathDXfromDowntown spA.pathDYfromDowntown...
            spA.lineDist spA.pathDist];
        
        totalPathDist=totalPathDist+spA.pathDist;
        
        sumNormTravelDist = sumNormTravelDist + (spA.pathDist ./ spA.lineDist);
    end
    
end

numValidPaths=size(outMatrix,1);

%write the header
fh=fopen(filename,'w');
fprintf(fh,'#downtownX, downtownY, pathDXfromDowntown, pathDYfromDowntown,  r, travelDist\n');
fclose(fh);

%write the data
dlmwrite(filename,outMatrix,'-append')

%write the overall summary
fh=fopen(filename,'a');
%fprintf(fh,'#\n');
fprintf(fh,['totalTravelDist=' num2str(totalPathDist) '\n']);
fprintf(fh,['roadLength=' num2str(mergedTree.totalLength) '\n']);
fprintf(fh,['sumNormTravelDist=' num2str(sumNormTravelDist) '\n']);
fprintf(fh,['n=' num2str(numValidPaths) '\n']);
fclose(fh);

disp('Finished pmWriteCSVforParetoFront')

end
