function [pathArrayTrimmed] = pmTrimPAOutliers(pathArray,largestn, smallestn)
%This function removes the largestn and smallestn (integers) number of paths, with respect to travel distance

%remove empty paths
pathArray=pathArray(~cellfun('isempty',pathArray));

%preallocation
dists=zeros(1,numel(pathArray));

%copy over pathdist
pathArrayTrimmed=pathArray;

for i = 1:numel(pathArray)
    
    dists(i)=pathArray{i}.pathDist;
    
end

%sort list in descending
[~,posn]=sort(dists,'descend');


%remove largestn number of paths
for i=1:largestn
    pathArrayTrimmed{posn(i)}=[];
end

%remove smallestn number of paths
for i=1:smallestn
    pathArrayTrimmed{posn(end-i+1)}=[];
end

end

