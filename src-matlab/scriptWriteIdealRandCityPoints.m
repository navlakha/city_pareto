%This script outputs a .csv file with the origin points of the ideal city
%Output is in Euclidian coordinates, units of km

filename='idealcity.csv';

rng(389745) %seed same every city

numrands=120;

trimtopn=5;
trimbottomn=0;

numfinalpts=numrands-trimtopn-trimbottomn;

randmaxdistKm=30;
randmindistKm=5;

pointsx=zeros(1,numrands);
pointsy=zeros(1,numrands);
pointdistM=zeros(1,numrands);

for i = 1:numrands
    
    distM=1000.*( (randmaxdistKm-randmindistKm).*rand() + randmindistKm);
    azDeg=360.*rand();
    
    trigAngleRad=deg2rad(-azDeg+90);
    pathX=cos(trigAngleRad).*distM;
    pathY=sin(trigAngleRad).*distM;
    
    pointdistM(i)=distM;
    
    pointsx(i)=pathX./1000;
    pointsy(i)=pathY./1000;
    
end

[~,posn]=sort(pointdistM,'descend');

for i = 1:trimtopn
    
    pointsx(posn(i))=NaN;
    pointsy(posn(i))=NaN;
    pointdistM(posn(i))=NaN;
    
end

for i = 1:trimbottomn
    
    pointsx(posn(end-i+1))=NaN;
    pointsy(posn(end-i+1))=NaN;
    pointdistM(posn(end-i+1))=NaN;
    
end

pointsx(isnan(pointsx))=[];
pointsy(isnan(pointsy))=[];
pointdistM(isnan(pointdistM))=[];

%write the file
outMatrix=[zeros(numfinalpts,1) zeros(numfinalpts,1) pointsx' pointsy' (pointdistM./1000)' (pointdistM./1000)' ];

%write the header
fh=fopen(filename,'w');
fprintf(fh,'#downtownX, downtownY, pathDXfromDowntown, pathDYfromDowntown,  r, travelDist\n');
fclose(fh);

%write the data
dlmwrite(filename,outMatrix,'-append')

%write the overall summary
fh=fopen(filename,'a');
%fprintf(fh,'#\n');
fprintf(fh,['totalTravelDist=' num2str(sum((pointdistM./1000))) '\n']);
fprintf(fh,['roadLength=' num2str(sum((pointdistM./1000))) '\n']);
fprintf(fh,['sumNormTravelDist='  num2str(numfinalpts) '\n']);
fprintf(fh,['n=' num2str(numfinalpts) '\n']);
fclose(fh);
