#!/usr/bin/env python

from __future__ import division

import sys,time
import utilities as UTIL
import networkx as nx
import numpy as np
import random
from os.path import basename # gets rid of path in filename

random.seed(10301949)

"""
    Greedy algorithm to minimize: alpha*(Road length) + (1-alpha)*(Travel distance)
    
        1. Build a tree T with the root.
        2. In each step, connect the unconnected node to T which minimizes the objective.

    Removes stuff added in v2 for branch points. 
    But added other nice outputs compared to v1, including multiR stuff.

"""

NUM_RANDOM=1000


#==============================================================================
#                                EVAL OBJECTIVE
#==============================================================================
def eval_objective(alpha,G,P2Coord,P2Label):
    return alpha*UTIL.compute_length(G,P2Coord) + (1-alpha)*UTIL.compute_droot(G,P2Coord,P2Label)


#==============================================================================
#                                 MAIN ALGORITHM
#==============================================================================
def doit(filename):

    # Compute road length and travel dist.
    P2Coord,P2Label,x0,y0,n,r = UTIL.read_city(filename)  
    
    print "n=%i" %(n)
    print "r=%.1f" %(r)


    # Run algorithm for different values of alpha.
    #for alpha in np.arange(0,1.00,0.02):
    for alpha in np.arange(0,1.00,0.01):

        # Initialize the tree and for each point compute its nearest neighbors.
        Tree = nx.Graph()
        NNs = {} # u -> [sorted nearest neighbors]
        for u in P2Label:
            if UTIL.is_downtown(P2Label[u]):
                Tree.add_node(u)

            # For each point, compute its sorted list of nearest neighbors.
            NNs[u] = []
            for v in P2Label:
                if u == v: continue

                NNs[u].append((UTIL.euclidean_dist(u,v,P2Coord),v))

            # Sort.
            NNs[u] = sorted(NNs[u])

            assert len(NNs[u]) == len(P2Label)-1 # each pt has distances to all except itself.

        
        # Find best edge to add: for each connected node only consider its nearest-neighbor.
        while Tree.order() < len(P2Label):
            best_obj = 10000
            best_uv = -1

            # For each node in the tree, find its neareset neighbor, then choose best.
            iTree = Tree.copy()
            for u in Tree:

                while True:
                    _,v = NNs[u][0] # don't pop because it may be used later.

                    if v in Tree: # was already added, move on.
                        _ = NNs[u].pop(0)
                        continue
                    else: # keep in list bc it might not be the best and may be used later.
                        break
                    

                assert u != v

                # Compute objective with the edge.
                iTree.add_edge(u,v)
                temp_obj = eval_objective(alpha,iTree,P2Coord,P2Label)

                # Check improvement.
                if temp_obj < best_obj:
                    best_obj = temp_obj
                    best_uv  = (u,v)
                iTree.remove_node(v)

                assert Tree.order() == iTree.order()
                assert Tree.size()  == iTree.size()

            u,v = best_uv # TODO: ties?
            assert u in Tree and v not in Tree
            assert not Tree.has_edge(u,v)
            
            Tree.add_edge(u,v)


        # Evaluate the alpha-tree.
        alpha_length = UTIL.compute_length(Tree,P2Coord)
        alpha_droot  = UTIL.compute_droot(Tree,P2Coord,P2Label)
        
        print "alpha=%.2f\t%.3f\t%.3f" %(alpha,alpha_length,alpha_droot)
        #UTIL.viz_tree(Tree,P2Coord,P2Label)

    print "city=%s\t%.3f\t%.3f" %(basename(filename),x0,y0)
    

    # Generate n-spokes at n=1,4,7,...,n.
    spokes = range(1,n,3)
    if spokes[-1] != n: spokes.append(n)

    for num_spokes in spokes:
        SpokeTree = nx.Graph()

        # Split points into 'num_spokes' continuous segments.
        spokes = np.array_split(xrange(1,n+1),num_spokes) # start at 1 bc 0 is downtown.
        for arr in spokes:
            for i in xrange(len(arr)-1):

                # Add linear chain.
                SpokeTree.add_edge(arr[i],arr[i+1])
                assert arr[i] != 0 and arr[i+1] != 0

            # Add point from downtown to centroid of the segment.
            mid = arr[int(len(arr)/2)]
            SpokeTree.add_edge(0,mid)

        UTIL.run_checks(SpokeTree)
        print "spokes=%i\t%.3f\t%.3f" %(num_spokes,UTIL.compute_length(SpokeTree,P2Coord),UTIL.compute_droot(SpokeTree,P2Coord,P2Label))


    # Generate random spanning trees. 
    # From: https://www.cs.cmu.edu/~15859n/RelatedWork/Broder-GenRanSpanningTrees.pdf
    nodes = Tree.nodes()
    assert len(nodes) == n+1
    for i in xrange(NUM_RANDOM):

        RandTree = nx.Graph()
        u = random.choice(nodes)

        RandTree.add_node(u)

        while RandTree.size() < n: # n points, 1 root.

            v = random.choice(nodes)

            if v not in RandTree:
                RandTree.add_edge(u,v)

            u = v

        UTIL.run_checks(RandTree)

        print "rand=%i\t%.3f\t%.3f" %(i,UTIL.compute_length(RandTree,P2Coord),UTIL.compute_droot(RandTree,P2Coord,P2Label))


    # Generate preferential attachment trees.
    for i in xrange(NUM_RANDOM):

        PATree = nx.barabasi_albert_graph(n+1,1) # node 0 (the hubbiest node) is downtown.

        # Relabel the nodes so that downtown becomes a random node.
        #random.shuffle(nodes)
        #mapping = {}        
        #for id,u in enumerate(PAxTree):
        #    mapping[u] = nodes[id]

        #PATree = nx.relabel_nodes(PAxTree,mapping)
        
        assert PATree.order() == len(nodes) == n+1        

        UTIL.run_checks(PATree)

        print "pa=%i\t%.3f\t%.3f" %(i,UTIL.compute_length(PATree,P2Coord),UTIL.compute_droot(PATree,P2Coord,P2Label))    


#==============================================================================
#                                    MAIN
#==============================================================================
def main():

    start = time.time()

    filename = sys.argv[1] # "../data/mtl15mini.csv"

    doit(filename)

    print "# time to run: %.2f mins" %((time.time()-start)/60)


if __name__ == "__main__":
    main()
