#!/usr/bin/env python

from __future__ import division

import matplotlib
matplotlib.use('Agg') # no locahost error.

import sys,re,os,math
from matplotlib import pylab as PP
import numpy as np

np.random.seed(10301949)

# Use R-style plotting.
import matplotlib.pyplot as plt
plt.style.use('ggplot')

PP.rc('font', **{'sans-serif':'Arial', 'family':'sans-serif'})


""" Creates pareto plot for city """


#==============================================================================
#                                    HELPER
#==============================================================================
def niceify(x,y):
    """ Return nicely formatted string representation of a list of floats in x and y. """
    s = ""
    for i in xrange(len(x)):
        s += " (%.2f,%.2f)" %(x[i],y[i])

    return s


def find_closest_alpha(alphas,cityx,cityy,paretox,paretoy):
    """ Finds the closest alpha to the Pareto (x,y) coordinates """

    min_dist    = 100000
    alphas_ties = []
    for i in xrange(len(paretox)):
        ix,iy = paretox[i],paretoy[i]

        d = math.sqrt( (ix-cityx)**2 + (iy-cityy)**2)

        # Check for ties.
        if abs(d-min_dist) < 0.001:
            alpha_ties.append(alphas[i])

        # Check for win.
        elif d < min_dist:
            min_dist = d
            alpha_ties = [alphas[i]]
            
    return np.mean(alpha_ties) # mean of all the ties.


def find_scale_factor(alphax,alphay,teshtx,teshty,r):
    """ Determine the amount the potato has to be scaled so that the city wins. """

    beat = False # does the city beat the potato?

    #for rscale in np.arange(0,50,0.25): # interpolate the front.
    for rscale in np.arange(0,1000,0.25): # interpolate the front.
    
        alphax_scaled = map(lambda x: x*(r+rscale)/r,alphax)
        alphay_scaled = map(lambda x: x*(r+rscale)/r,alphay)
        for i in xrange(len(alphax)): # go through each alpha at the curr level of interpolation.
            ix,iy = alphax_scaled[i],alphay_scaled[i]

            if teshtx < ix and teshty < iy:
                beat = True
                break

        if beat:
            break

    # It must've won by now.
    #if not beat: print filename
    assert beat

    return rscale,alphax_scaled,alphay_scaled



#==============================================================================
#                                 MAIN ALGORITHM
#==============================================================================
def doit(filename):

    # Print header.
    #print "#filename\tcity\thway\tradius\tcity_scaled\talpha_pot\talpha_scl\tcityx\tcityy\trand_scaled\tpa_scaled"

    # Process the file.
    assert "rand" in filename

    # Get city stats of file.
    match = re.search('([a-z][a-z][a-z])_rand_(\D+)_(v[0-9])',filename)
    city,hway,ver = match.group(1), match.group(2), match.group(3)        
    
    # Go through the file
    alphas,alphax,alphay = [],[],[]
    spokesx,spokesy = [],[]
    randx,randy = [],[]
    pax,pay = [],[]
    with open(filename) as f:
        for line in f:
            if line.startswith("n="): # number of points.
                n = int(line.strip().split("=")[1])

            elif line.startswith("r="): # radius.
                r = float(line.strip().split("=")[1])

            elif line.startswith("alpha="): # alpha values.
                cols = line.strip().split("\t")

                alphas.append(float(cols[0].split("=")[1]))

                # - norm by r and n.
                alphax.append(float(cols[1]) / r )
                alphay.append(float(cols[2]) / (n*r) )
                

            elif line.startswith("city="): # city values.
                cols = line.strip().split("\t")

                # - norm by r and n.                    
                cityx = float(cols[1]) / r
                cityy = float(cols[2]) / (n*r)

            elif line.startswith("spokes="):
                cols = line.strip().split("\t")

                # - norm by r and n.                    
                spokesx.append(float(cols[1]) / r)
                spokesy.append(float(cols[2]) / (n*r))

            elif line.startswith("rand="):
                cols = line.strip().split("\t")

                # - norm by r and n.                    
                randx.append(float(cols[1]) / r)
                randy.append(float(cols[2]) / (n*r))

            elif line.startswith("pa="):
                cols = line.strip().split("\t")
                
                # - norm by r and n.                    
                pax.append(float(cols[1]) / r)
                pay.append(float(cols[2]) / (n*r))

            elif line.startswith("# time to run"): continue

            else: assert False


        # Find the rscale for the city.
        rscale,alphax_scaled,alphay_scaled = find_scale_factor(alphax,alphay,cityx,cityy,r)


        # Find the closest alpha on the Pareto fronts.
        closest_alpha_pareto = find_closest_alpha(alphas,cityx,cityy,alphax,alphay)
        closest_alpha_scaled = find_closest_alpha(alphas,cityx,cityy,alphax_scaled,alphay_scaled)


        # Find the rscale for random and preferential attachment.
        rand_rscale,_,_ = find_scale_factor(alphax,alphay,np.mean(randx),np.mean(randy),r)
        pa_rscale,_,_ = find_scale_factor(alphax,alphay,np.mean(pax),np.mean(pay),r)

        # Output statistics.
        print "%s\t%s\t%s\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f" %(filename,city,hway,r,r+rscale,closest_alpha_pareto,closest_alpha_scaled,cityx,cityy,r+rand_rscale,r+pa_rscale)

        
        # prints title without "h/nh" and "r" and with eps instead of scale factor.
        PP.title(r"$\mathrm{%s}\ \ \epsilon=%.2f\ \ \alpha=%.2f$" %(city.upper(),(r+rscale)/r,closest_alpha_scaled),fontsize=24) # distance to pareto front.


        # Plot Pareto curve.
        PP.plot(alphax,alphay,c='grey',marker='s',linewidth=3,markersize=8,zorder=1)
        
        PP.plot(alphax_scaled,alphay_scaled,c='grey',marker='s',linewidth=3,markersize=8,zorder=1,alpha=0.1)
        
        # Plot city.
        PP.scatter(cityx,cityy,c="red",marker='x',linewidth=3,s=300,zorder=2)

        # Plot spokes.
        #PP.plot(spokesx,spokesy,c='blue',marker='.',linewidth=3,markersize=8,zorder=1)
        
        # Plot random.
        #PP.scatter(randx,randy,c='#6495ED',marker='+',linewidth=1,s=100,zorder=2)
        
        #PP.xlabel("Road Length (km) / r",fontsize=24)
        #PP.ylabel("Travel Distance (km) / (nr)",fontsize=24)

        PP.xlabel("Normalized Road Length",fontsize=24)
        PP.ylabel("Normalized Travel Distance",fontsize=24)

        # Set the x and y limits without the rands in there.
        axes = PP.gca()
        xmin,xmax = axes.get_xlim()
        ymin,ymax = axes.get_ylim()
        PP.xlim(0,xmax)
        PP.ylim(0,ymax)
        
        
        #Remove borders on top and right:
        ax = PP.gca()
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        ax.xaxis.set_ticks_position('bottom')
        ax.yaxis.set_ticks_position('left')

        #Increase label size:
        ax = PP.gca()
        ax.tick_params(axis='both',which='major',labelsize=24)


        # Save
        PP.savefig(filename+"_potato.pdf",transparent=False,bbox_inches='tight')
        PP.close()


#==============================================================================
#                                    MAIN
#==============================================================================
def main():

    filename = sys.argv[1] # "abo_rand_h_v5.csv_alpha.txt"
    
    doit(filename)


if __name__ == "__main__":
    main()
