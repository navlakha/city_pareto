#!/usr/bin/env python

from matplotlib import pylab as PP
import networkx as nx
import math,re
import numpy as np


#==============================================================================
#                                    VIZ TREE
#==============================================================================
def is_point(label):    return label == "point"
def is_branch(label):   return label in ["stem","branch"]
def is_downtown(label): return label == "downtown"

def viz_tree(G,P2Coord,P2Label):
    """ Displays plant/tree visualization. """

    node_size,node_color = [],[]
    pos = {}
    for u in G:

        pos[u] = (P2Coord[u][0],P2Coord[u][1])

        if is_downtown(P2Label[u]):
            node_color.append('black')
            node_size.append(350)
        elif is_branch(P2Label[u]):
            node_color.append('black')
            node_size.append(0)
        elif is_point(P2Label[u]):
            node_color.append('green')
            node_size.append(250)

    #nx.draw(G,pos=pos,arrows=False,with_labels=True,node_size=node_size,node_color=node_color,edge_color="brown",width=4,font_size=12,font_color='red',font_weight='bold')
    nx.draw(G,pos=pos,arrows=False,with_labels=False,node_size=node_size,node_color=node_color,edge_color="brown",width=4,font_size=12,font_color='red',font_weight='bold')
    PP.draw()
    PP.show()
    #PP.savefig("alpha1.0.pdf")
    PP.close()


#==============================================================================
#                                  ERROR CHECKING
#==============================================================================
def run_checks(G):
    """ Error checking on G. """

    # Check if there are any isolated nodes.
    if nx.number_connected_components(G) > 1: assert False

    # Check if truly a tree.
    if G.order() != G.size() + 1: assert False


#==============================================================================
#                                 READ CITY ROADS
#==============================================================================
def read_city(filename):
    """ Read Suen's input file. """

    P2Label = {} # points to label
    P2Coord = {} # points to (x,y)
    rs      = [] # list of r values for all points.
    multiR = False

    with open(filename) as f:
        for point,line in enumerate(f):
            if line.startswith("#"): # header.

                # Good as time as any to add downtown.
                assert point == 0
                P2Coord[point] = (0,0)
                P2Label[point] = "downtown"

            elif line.startswith("totalTravelDist"):
                travel_dist = float(line.strip().split("=")[1])

            elif line.startswith("roadLength"):
                total_length = float(line.strip().split("=")[1])

            elif line.startswith("n"):
                n = int(line.strip().split("=")[1])

            elif line.startswith("sumNormTravelDist"):
                multiR = True
                
            else:

                # Split by "," to get the coordinates.
                cols = line.strip().split(",")
                assert int(cols[0]) == 0 and int(cols[1]) == 0 # downtown is at (0,0)
                assert len(cols) == 6

                # Get (x,y) coordinate of the point.
                x,y = float(cols[2]),float(cols[3])

                # Get radius: For multiR, use mean r. For non-multiR, all r are the same.
                #if point == 1: r = int(cols[4])
                #if point > 1: 
                #    if int(cols[4]) > r:
                #        r = int(cols[4])

                #rs.append(int(cols[4])) # this was before we did random points, where r = float.
                rs.append(float(cols[4]))
                                    
                # Add point.                
                assert point not in P2Coord and point not in P2Label and point > 0
                P2Coord[point] = (x,y)
                P2Label[point] = "point"

    if not multiR:
        # all the r's are the same.
        assert len(set(rs)) == 1
        r = rs[0]
    else:
        r = np.mean(rs)

    return P2Coord,P2Label,total_length,travel_dist,n,r


#==============================================================================
#                             COMPUTE LENGTHS
#==============================================================================
def euclidean_dist(p1,p2,P2Coord):
    """ Computes the Euclidean distance between the two points. """

    x1,y1 = P2Coord[p1]
    x2,y2 = P2Coord[p2]

    return math.sqrt( (x1-x2)**2 + (y1-y2)**2)


def compute_length(G,P2Coord):
    """ Computes the total length of the plant. """

    return sum([euclidean_dist(u,v,P2Coord) for u,v in G.edges_iter()])

    
def compute_droot(G,P2Coord,P2Label):
    """ Computes the total distance from each leaf to the root. """

    # Find the root.
    for u in G:
        if is_downtown(P2Label[u]):
            root = u
            break

    # Compute distance from each leaf to the root.
    droot = 0
    for u in G:
        if is_point(P2Label[u]):
            path = nx.shortest_path(G,root,u)
            for i in xrange(len(path)-1):
                droot += euclidean_dist(path[i],path[i+1],P2Coord)

    return droot

