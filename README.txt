Data and code for: 

    J. Y. Suen and S. Navlakha. Travel in city road networks follows similar trade-off transport principles as neural and plant arbors. J. R. Soc. Interface, 2019.

    Jon Suen (jsuen@ece.ucsb.edu)
    Saket Navlakha (navlakha@salk.edu)


==================================================================
CITY PATHS

City paths data, as obtained from Google Maps API is provided in MATLAB .mat files in the data-mat directory. Each file is named after the 3-letter abbreviation listed in the supplemental information.

Each .mat file contains the travel paths retreived (pathArray) as well as a tree consisting of the merged paths forming the road tree (mergedTree). Matlab code is provided containing the algorithms used to process these variables and extract the origin and destination points for the Pareto optimality analysis.

Code Description:
    pmMakeTree                      Takes a pathArray and merges it to form a mergedTree
    pmRemoveOverlapPath2            Function called by pmMakeTree to determine if a path overlaps
    pmRemoveOverlapPath3            Optimized version of pmRemoveOverlapPath2, used in paper
    pmTrimPAOutliers                Trims longest and shortest paths in a pathArray 
    pmWriteCSVforParetoFront        Produces the CSV file necessary for the Python Pareto analysis
    pmWriteCSVTrafficStatsForParti  Produces the CSV file necessary for ParTI analysis
    scriptWriteIdealRandCityPoints  Writes a CSV file describing the ideal city origin points

Data Description: 
The outputs for these functions using the publication dataset are included in the data-csv directory. The formats are described in their respective generation functions:
    idealcity.csv                   Output from scriptWriteIdealRandCityPoints
    trafficstats.csv                Output from pmWriteCSVTrafficStatsForParti
    aaa_rand_h_v5.csv               Output from pmWriteCSVforParetoFront for the corresponding city



==================================================================
PARETO OPTIMALITY ANALYSIS

Python code to find the Pareto front is in the src-python directory.

To perform the Pareto optimality analysis, run the following two commands. An example city input file is included in this directory:

1. Generate the Pareto front:

    ./generate_pareto_fast3.py abo_rand_h_v5.csv > abo_rand_h_v5.csv_alpha.txt


2. Plot the Pareto front: 

    ./analyze_pareto.py abo_rand_h_v5.csv_alpha.txt



==================================================================
CITY MAT FILE DESCRIPTION

There are three variables inside each MAT file in the data-mat directory, where AAA represents the abbreviation of the city. The data retreived from Google Maps is in the pathArray variables. The pathArray was then run throguh pmMakeTree to produce the mergedTree variable.

mergedTreeAAArand
    This is the tree with overlapping travel path segments removed.

    branchLat/branchLong cell array. Each cell has an array corresponding to the WGS84 lattitudes and longitudes for the points forming the branch.
    numTreeBranches = Scalar, number of tree branches
    branchDist = Array containing the length of each branch in km
    totalLength = Scalar, total length of tree in km

pathArrayAAArand
pathArrayAAArandTrimmed
    These two cell arrays are the same except the Trimmed version has paths that fail quality checks deleted. The trimmed version has exactly 120 paths, since paths were acquired until 120 valid paths were obtained.

    Quality criteria are path includes a ferry, snapping exceeds threshold distance, path travels through specific infeasible paths (bodies of water), and paths which exceed 10 times the straight-line distance.

    Each cell array contains a struct describing a single travel path. The struct contains:

    fromAddr, toAddr: String with the geocoded origin and destination points as returned by Google Maps
    fromLat, fromLong, toLat, toLong: Desired origin and destination points, passed to Google Maps API in WGS84 coordinates (unsnapped)
    
    departureTime: UNIX timestamp of the departure time passed to Google Maps API
    
    pathDist: Travel distance on km
    idealLineDist: Ignore this variable, see lineDist
    
    travelTime: Travel time in minutes, without traffic, as returned by Google Maps API
    travelTimeTraffic: Travel time in minutes, with traffic, as returned by Google Maps API
    
    rawJSON: The raw JSON path returned by Google Maps API

    pathDistArray: An array of the distance of each leg of the path, in km
    pathDurArray: An array of the travel time of each leg in the path, in minutes
    pathInstructionArray: A cell array containing travel instruction strings
    pathManueverArray: A cell array of strings of specific manuevers, e.g. turn-left, for each leg
    pathStartLocArray: Structures containing lat and lng of each leg's start point
    pathEndLocArray: Structures containing lag and lng of each leg's end point
    pathPolyLineArray: A cell array of strings containing the coded path polyline, as returned by Google Maps API

    fromLatS/fromLongS: Origin lat/long after snapping to the nearest road by the Google Maps API

    bearing: Straight-line compass bearing from the origin to the destination, in degrees
    lineDist: Straight-line distance from the origin to the destination
    pathDeltaX/pathDeltaY: The cartesian X and Y-axis distances, in km, of the straight-line path

    goingDowntown: always true, for travel torwards downtown

    pathDXfromDowntown/pathDYfromDowntown: same as pathDeltaX/pathDeltaY except from downntown to the origin
    bearingfromDowntown: same as bearing, except from downtown to the origin, in degrees
    bearingfromDowntownTrig: same as bearingfromDowntown but as a trigonometric angle (0=East, 90=North)
